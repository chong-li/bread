# Massa de macarrão fresca

### Ingredientes
- 100 g farinha → 100% farinha
- 60% molhados → ovos, gemas e água
- 2% sal

Receita rende uma porção individual em uma refeição com acompanhamentos. Até 400g consigo sovar a massa no processador em um batch. Mais que isso precisa dividir em dois e juntar no final. Se for precisar separar em 3 você está exagerando

## Farinha

Reza a lenda que a 00 é a melhor pra isso, por enquanto, o amadorismo faz eu usar a normal mesmo. Recomendo só mudar se tiver certeza.

## Molhados

Bom, a receita dá uma porcentagem de 61,6667% de molhados (razão de 185g:300g). Isso dá quase 1 ovo por porção. Pra `n` pessoas, temos as quantidades calculadas para farinha (`F`), ovos (`Ov`) e água (`A`):
```
F ← n⋅100g
Ov ← n ovos, medidos em g
A ← (37⋅F)/60 - Ov ~= 0.617⋅F - Ov
```

A receita original também diz pra colocar umas gemas a mais. Eu acho desnecessário desperdiçar essas claras.

O cálculo pode ser feito ao contrário. Separe um ovo por pessoa e um ovo extra e pese. Essa medida é 61,7% a calculamos a quantidade de farinha daí, pra `n` pessoas:
```
Ov ← n+1 ovos, medidos em g
F ← (60⋅Ov)/37 ~= 1,621⋅Ov
```

Essa proporção foi pensada pra cozinheiros amadores (como eu, e provavelmente quem mais estiver lendo isso). A ideia é começar com uma proporção alta de molhados. Normalmente a massa vai ficar boa com algo entre 55% e 60% de molhados (depende muito da farinha).

A vantagem de começar com uma proporção propositalmente alta de molhado é ajustar acrescentando secos em vez do contrário. É muito mais fácil incorporar farinha numa massa grudenta do que água numa massa seca. Confia, eu errei no sentido contrário vezes o suficiente, não é legal.

## Sova

Misturar tudo no processador usando a lâmina, a pazinha não aguenta. Limpar tudinho acumulando na massa (sim... *tudinho*)

Sovar com movimentos curtos/suaves por uns 10min. A ideia de fazer movimentos curtos é evitar rasgar a massa (o que você pode acabar fazendo afundando um dedão na massa ou atritando a massa demais na bancada). Um jeito bom é tentar dobrar um bom tanto de massa da bancada pro meio da parte da massa que está virada pra você, girar um pouquinho a massa e repetir o processo. Pode ficar um vulcãozinho no meio. Não tem problema (o vídeo da Helen Rennie no YouTube é boa referência: `Helen Rennie pasta`)

Deixar descansando (em "autólise") por pelo menos 30 min.

## Abertura da massa

#### Com aparelho

Dividir a massa aproximadamente por n partes. Amassotar antes de tentar deixar num formato retangular. Farinhar sempre que grudento.

No nosso aparelho, é difícil acertar essa primeira grossura de acerto. Isso faz com que seja difícil dobrar em 3 partes e passar na máquina. Então podemos fazer em 2 dobras sempre e passar mais vezes. Sem galho.

#### Sem aparelho

Nesse caso recomendo fazer orichiette (orelhinhas). A ideia vai ser pegar um teco de massa aproximadamente do tamanho de um dedão (talvez do dedinho pra quem tem mão grande ou dedos grossos), apertar na  bancada fazend movimentos circulares. Se fizer bem toscamente e com desprezo vai ficar parecendo uma orelhinha em vez de uma combuquinha. Displiscência é o nomo do jogo aqui. Quanto mais desprezo em fazer mais gostoso vai ficar. É igualzinho Sazón, só que ao contrário.

## Cozimento

Cozinha mais rápido, bem mais. Precisa de sal na água nesse caso. Se a massa parecer um pouco salgada demais na panela, tá certinho.
