# Béchamel

### Ingredientes

 - Manteiga: 1/2 xícara de manteiga
 - Farinha de trigo: 1/2 xícara
 - Leite: 1L

Tempeiro a gosto, mas é um molho simples, então não fode.
Tradicionalmente usa-se sal e noz-moscada, por vezes alguma erva.
Chef John gosta de por pimenta caiena, mas ele é um "Cayenne kind of guy".

O importante aqui é notar que a quantidade de manteiga e farinha é 1:1.
Isso está assim pra reservar esse tanto de farinha.
Talvez não use tudo.

### Preparo

Molho simples, não fode inventando moda. Ideia é começar fazendo um Roux.

Roux é uma *pastinha* que faz com manteiga e farinha de trigo.

A ideia é começar a derreter a manteiga até ela dar aquela espumada.
Depois, acrescentar farinha, um pouquinho de cada vez, mexendo com um fouet.
Pode fazer isso até ele parecer uma pasta bem seca, mas antes de parecer uma farofa
(evite até texturas aparentando farofas molhadas)

Pode deixar isso dar uma *tostadinha*, bem de leve.
Isso deve dar um toque de defumado/umami, mas também vai mudar a cor pra algo mais escuro,
se alguém se importar com isso.
Um fator importante é que quanto mais torradinho menor a emulsificação. 

Quando estiver no ponto desejado, comece a adicionar o leite frio, sempre mexendo com o fouet.
Em duas vezes já tá bom.
A primeira mais comedida pra incorporar o Roux no leite, depois pode por o resto.
Só põe o resto com calma pra não desandar.
Cozinha caseira é maratona, não é corrida!
