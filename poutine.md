# Poutine

## Ingredientes

 - Queijo "de coalho" → ~80g por pessoa
 - Batata "frita" → De 200g a 300g por pessoa
 - Gravy (molho de carne) → De 100ml a 150ml por pessoa
 - Cheiro verde a gosto (só cebolinha não fica devendo tanto quanto só salsinha IMHO)

## Queijo de Coalho

Originalmente, se usa cheese curd pra fazer.
Esse quiejo é um tipo de quijo de coalho,
mas ele é diferente do que costumamos encontrar no Brasil.
Em particular, parece que o curd tem maior conteúdo de gordura.
Em princípio isso deve impactar em como o queijo derrete.

Levando isso em conta, nosso plano aqui é experimentar algumas opções e ir
catalogando os resultados.
Dessa forma cada um pode escolher como prefere.

### Mussarela Bolinha/Palito

Mussarelinhas classudas parecem ir bem.
Tanto pode ser bolinha quanto palito ou cabaça.
De toda forma, a recomendação é dar uma *desfiada*,
mas mantendo uma espessura bem generosa.
No caso do palito pode ser melhor não desfiar nada.ç

Outro cuidado legal é o de tirar da geladeira um bom tempo antes de servir.
Idealmente o queijo vai estar em temperatura ambiente pra receber o gravy.


### Queijo de coalho mesmo

Esse ainda está sem teste.
Pretendemos testar com coalho pra tapioca em vez de usar os picolés de coalho
que são feitos pra churrascos.
A impressão é que os picolés tostam bem mas não derretem tão bem.

Acho que tapiocas são bons termômetros.
Se você não vê diferença em usar o picolé pra fazer uma tapioca,
acho que tanto faz pra você.
Em princípio parece legar cortar em nacos generosos.

Um cuidado legal é o de tirar da geladeira um bom tempo antes de servir.
Idealmente o queijo vai estar em temperatura ambiente pra receber o gravy.

## Batata "*Frita*"

Temos várias opções de como fazer aqui.

Mas a observação importante é que a ideia é afogar a batata em gravy.
Então não importa se a batata sai crocantinha da fritura,
ela vai ficar meio molenguinha de toda forma.

Então, decidimos que o que vale a pena é fazer do jeito que for mais prático.

### Método Rita Lobo de fritura fácil

Corta as batatas no formato desejado,
deixando ou tirando a casca de acordo com a preferência do freguês.
Seca o quanto der (e se não der desencana).
Pode ser com pano ou toalha de papel.
Não recomendo deixar secar ao ar pois isso deve oxidar a superficie da batata e afetar o gosto
(mas se experimentar assim e não sentir diferença me avisa por favor)

Bota óleo numa panela, taca as batatas cortadas lá dentro, frias mesmo.
Liga o fogo no alto até você começar a ficar com medo de deixar no fogo alto.
A partir desse ponto pode abaixar um pouco e ficar de olho até as batatas dourarem.

### Batata Assada

É, pois é.
A gente aqui tem certeza que é melhor assar as batatas.
Vai fazer menos sujeira, não vai gastar tanto óleo.
Também não vai ficar tão crocante,
mas daí é só lembrar que tudo vai acabar em gravy que tudo bem

**Ainda precisamos testar algum método de batata de forno pra atualizar aqui**

## Gravy

p/ o gravy
 - 1:1 manteiga:farinha (roux)
 - 1:10 roux: caldo de carne