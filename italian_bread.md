# Pão Italiano com Levain

#### INGREDIENTES
 - 100% farinha
 - 30% fermento
 - 60% - 70% água
 - 2% sal

A quantidade de farinha determina o valor de referência e os outros infgredientes são calculados em relação a essa quantidade. Para calcular a quantidade de sal numa receita com 500g de farinha, por exemplo, calcula-se 2% de 500g de forma que a receita pede por 10g de sal. Podemos usar isso pra calcular quanto rende uma receita, inclusive. Some as porcentages e veja quanto isso dá em Kg (mas saiba que isso vai superestimar o peso por conta de evaporação de água).

Para um pão de aprox. 1Kg as quantidades são:
- 500g farinha
- 150g fermento
- 350g água (70% de hidratação)
- 10g sal

Para outras quantidades a estimativa é metade da massa pretendida de pão para a massa de farinha. Essa estimativa é mais "exata" para 70% de hidratação, mas não vale a pena fazer diferente porquê a evaporação enviesa e aumenta a variância. Mas também é sempre possível decidir só a quantidade de farinha e ver que tamanho de pão fica.

_Observação sobre hidratação_. Quanto maior a hidratação mais difícil de sovar (porque a massa será mais grudenta) mas mais aerado o pão. Ciabatas são gostosas e levinhas porque tem alta hidratação. Pra padeiros amadores incompetentes, é bom começar com hidratações mais baixas (i.e. 60%) e ir almejando a ciabata.

## Pré Fermento

#### INGREDIENTES

- `pré_fermento == levain + farinha + água`

Receita base pede 150g de pré fermento pra um pão de 1Kg. Podendo ser 80g de farinha e 70g de água.

#### Ajustando as Quantidades

Se a receita pede por L gramas de pré fermento, divida essa quantidade em 2 partes iguais de farinha e água (digamos Fp e Ap). Depois, tire um épsilon gramas da quantiadde de água.

```
Fp ← Ap ← L/2
Fp ← Fp + epsilon
Ap ← Ap - epsilon
```
Epsilon pode ser da ordem de 5g a 10g. Mas não há necessidade de precisão. Fica de boa e aproxima essas quantidades que funciona.

Existe bastante espaço pra correções, se necessário. Vai ser importante, no entanto, pra fechar as quantidades de ingredientes, saber os valores em gramas de `Ap` e `Fp`, pois essas quantias serão relevantes no cálculo das quantias necessárias em outros passos.

_Observação_: Pra facilitar contas, é possível usar no pré-fermento a mesma proporção decidida para a hidratação do pão. Isso deve alterar o tempo que demora pro pré-fermento chegar no ponto. Isso também vai aumentar a quantidade de água a ser colocada em autólise, o que pode ser a grande vantagem

#### PROCESSO
Adicione a água e a farinha ao levain e misture até homogeneizar. Deixe à temperatura ambiente até que a mistura dobre de tamanho. Esse processo leva algumas horas. Ainda assim, e bom ficar de olho pra não deixar a mistura começar a esvaziar. Se isso acontecer a coisa toda passou do ponto.

Não se especifica o tempo necessário pra dobra de tamanho pois isso é dependente da temperatura ambiente. Em dias mais quentes o processo é mais rápido. Em dias mais frios, mais lento.

## Autólise

O processo de autólise é o de misturar a farinha e a água, ainda algum tempo antes de adicionar o fermento e sovar a massa. Fazemos a autólise pra adiantar o processo de formação de rede de glúten.

Na receita base de pão de 1Kg com 70% de hidratação, e considerando pré-fermento com 80g de farinha e 70g de água, a autólise será composta por 500g de farinha e 336g de água.

_Pro tip_: Adicionar o sal na autólise. Tecnicamente deixa de chamar autólise, mas uma rosa ainda é uma rosa por qualquer outro nome. Essa é a maneira mais fácil de acrescentar e incorporar o sal. A razão pela qual se desaconselha é que o processo de autólise tem o objetivo de pré hidratar a massa para que as proteínas que compõe o glúten possam entrar em contato e já ir começando o processo de formação do glúten. Como sal é higroscópico, adicionar o sal nesse ponto reduz a efetividade da autólise. Dito isso, a observação é que a quantidade de sal adicionada é baixa o suficiente pra não afetar muito esse processo. Recomendado de verdade é testar por si pra descobrir a preferência.

#### Ajustando as Quantidades

A receita pede 500g de farinha e, digamos, 70% de água. Em princípio isso faria com que precisássemos de 350g de água. No entanto, utilizamos farinha e água no pré fermento. Essas quantidades devem ser contabilizadas.

A receita base pede 500g de farinha, assim sendo, acrescentamos aos 500g de farinha a farinha usada no pré-fermento e obtemos um novo valor de 100%. No caso:
```
100% ← 500 + Fp
```
Então, como precisamos de 70% de água nesse exemplo, precisamos de `0,7⋅(500 + Fp)` gramas de água. Não podemos nos esquecer que também adicionamos `Ap` gramas de água ao pré fermento.

Juntando tudo, as quantidades que precisamos utilizar de farinha e água na autólise (Fa e Aa, respec.) são:
```
Fa ← 500
Aa ← 0,7⋅(500 + Fp) - Ap
```
Agora, generalizando a fórmula para a quantidade de água, utilizando a quantidade de farinha (`Fa`) e a porcentagem de hidratação (`H`, i.e 70%→`H==0.7`)
```
Aa ← H⋅(Fa + Fp) - Ap
```
#### PROCESSO
Misturar as quantidades de farinha e água num pote grande (2L mais ou menos) até hidratar tudo. Pode ser difícil de hidratar a farinha toda. Com probabilidade altíssima será necessário misturar com a mão pra chegar no ponto. Depois é só deixar descansar até a hora que o pré fermento estiver no ponto.

## Sova

Após ter deixado a massa em autólise por um bom tempo (pelo menos 30 min, idealmente 2h, pragmaticamente é bom fazer logo após alimentar o fermento), misturar massa, fermento e sal. Depois é só sovar.

#### PROCESSO

 - Na panificadora: deixar rolar o ciclo de massa, dando umas cutucadas pra massa "pescar" o que fica nos cantinhos da máquina.
 - Na mão: melhor colocar a massa na bancada, depois pesar o fermento e colocar por cima, depois acrescentar o sal. Colocar um timer pra não sovar demais.

_Na mão_:
 - Existem algumas técnicas diferentes. Muito difícil passar isso por escrito, YouTube tutoriais disso (kneading e slap and fold são bons termos pra começar uma busca).
 - Pequenas pausas (5min) para deixar formar um pouco do gluten e grudar menos podem ser uma boa. Algum esforço pra não exagerar nas pausas pode ser uma boa.
 - Pra quem está começando, pode ser uma boa ideia começar com pães com hidratação menor pra ir se acostumando. Hidratação menor vai deixar a coisa menos grudenta, mas faz um pão menos fofinho e aerado.
 - Depois que a massa ficar "lisinha" (muitas aspas aí, fica menos lisinha que na modelagem), unte um pote com azeite pra deixar a massa descansar.
 - Colocar a massa no pote com a parte mais lisinha para baixo. **Importante**, a parte lisinha para baixo presume a execução de dobras. Se não for ter dobras é precisamente o contrário.

**Observações pessoais sobre sova**:
 Eu acho *slap and fold* difícil porque gruda muito.A ponto de prejudicar, inclusive, porquê ao tentar desgrudar a massa vai desgarrando e quebrando as "fibras" que deveriam estar se formando. Recentemente, comecei a experimentar fazer um *slap and roll*. Em vez de tentar dobrar a massa por cima de si mesma como parece que o pessoal faz eu jogo a massa por cima de si mesma... com desprezo

 Legal assistir os vídeos de sova do Jack antes de sovar novamente porquê a técnica dele é bem amigável


## Dobra

A cada 30min após a sova, podemos fazer as dobras. Fazemos isso umas 2 ou 3 vezes (às vezes mais). Quanto mais molenga a massa estiver mais dobras fazemos.

#### PROCESSO
Vamos repetir o mesmo processo 4 vezes (1 vez por "ponto cardeal"). Pescar a lateral da massa, puxar até onde ela vai e dobrar por cima do resto.

Esse processo vai "alisando" a parte de baixo da massa. Mas é interessante que a parte lisa seja a parte que segure o crescimento da massa na 1ª fermentação. Dessa forma, o recomendado é, após a última dobra, virar a massa pra apontar a parte mais lisa pra cima.

## 1ª Fermentação

Após a dobra, deixamos a massa realizar a 1ª fermentação. Essa parte pode acontecer dentro da geladeira ou fora.

#### PROCESSO

Na geladeira, pode deixar a massa fermentando pela noite. No dia seguinte observar se a massa deu uma crescida. Se não, pode deixar ela terminar de realizar essa fermentação em temperatura ambiente ou mantê-la na geladeira mesmo.

## Modelagem

Modelagem é o processo de moldar a massa pra que ela tenha a estrutura certa para o formato desejado.

#### PROCESSO

Depende muito do formato do pão. Para pães em forma de bolinha a ideia é ir escondendo os cantinhos da massa pra parte de baixo. Muito difícil explicar isso. Aninha recomendou um vídeo pra começar a ver diferentes maneiras de fazer. Assitir alguns desses antes de refazer é uma boa

Ao fim do processo de modelagem, colocar a massa pra descansar com a parte mais lisinha pra baixo. Pode ser no banneton ou num escorredor de macarrão, dependendo do tamanho da massa.

Se a massa parecer meio rasgadinha ou quebradinha na parte que ficou pra cima, melhor dar umas pinçadinhas pra juntar a massa.

Para massas mais hidratadas, é bom fazer o processo de modelagem 2 vezes. Na primeira pode fazer como se fosse modelar uma bola. Na segunda faz a modelagem pretendida.

## 2ª Fermentação

A segunda fermentação é a última antes de levar ao forno.

#### PROCESSO

Tomar cuidado pra massa não ressecar. Se for cobrir com pano, dar uma umedecida. Ainda assim, é bom colocar um plástico por fora pra evitar evaporação da água na massa.

A 2ª fermentação leva em torno de 2h fora da geladeira. Mas é bom ficar de olho no volume da massa e na consistência da mesma. Quando a massa ficar no ponto ela deve ter dobrado de tamanho (ou algo próximo disso). Pra verificar que a massa está no ponto, dê uma chaqualhadinha pra ver se ela está mais molenga e faça o teste da cutucada. Se a massa estiver no ponto ela vai voltar ao formato inicial prontamente.

## Assar

Agora é só assar e correr pro abraço.

#### PROCESSO

Pré aquecer o forno na temperatura máxima por uns 15 min.
 - **Na caçarola**: deixar a caçarola pré aquecendo junto.
 - **Na assadeira de pizza**: deixar uma assadeira pré aquecendo junto.
 - **No deque oven**: colocar 250°C em cima e em baixo, deixar 15min depois de chegar na temperatura antes de por o pão.

Antes de colocar no forno:
- **Na caçarola**. Virar a massa no baking sheet. Fazer os cortes para depois colocar tudo isso na caçarola. Borrifar água na tampa da caçarola pra crocrância adicional.
- **Na assadeira de pizza**. Virar a massa na assadeira. Fazer os cortes e colocar tudo na assadeira. Logo antes de colocar a massa no forno, colocar água na assadeira.
- **No deque oven**: Dar uma borrifada do vapor. Virar a massa na pá. Dar outra borrifada. Fazer os cortes. Dar uma última borrifdada antes de colocar pra dentro. Imediatamente abaixar a temperatura de cima para 100°C e a de baixo para 200°C

Assar o pão em temperatura alta até ele crescer. Isso demora uns 15~20 min.
- **Na caçarola**. Tentar ficar de olho pra ver se parece que o pão está crescendo.
- **Na assadeira de pizza**. Borrifar água nos cortes de tempos em tempos. Pode borrifar cruelmente. Tente alagar os cortes até parecer que você estragou o pão afogado.
- **No deque oven**: Nos primeiros 15min, manter saturado de vapor dando umas borrifadas com frequência. Depois só ficar de olho pros próximos 15min

Finalize com uns 15~20 min em temperatura baixa (mas não mínima), até estar bem douradinho.

## Descansar

Depois de assado, é importante deixar o pão descansando em temperatura ambiente. Ele está terminando de assa a parte de dentro. Pode esperar a casca estar na temperatura ambiente antes de abrir o pão.
